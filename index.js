require("dotenv").config();
const express = require("express");

// initialization import statement
const app = express();

// Morgan is a Logger
const morgan = require("morgan");

//Import Routing
const router = require("./routes");
const flash = require("connect-flash");
// const cors = require("cors");
const session = require("express-session");

// PORT with the default 3000
const PORT = process.env.PORT;

// Express Configuration Basis
app.locals.moment = require("moment");

// Middleware to Parse JSON
app.use(express.json());
app.use(flash());
app.use(
  session({
    secret: "keyboard cat",
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false, maxAge: 60000 },
  })
);
app.use(express.urlencoded({ extended: false }));

// Path in PUBLIC Directory
app.use(express.static("public"));

// Set View Engine
app.set("view engine", "ejs");

// For Logger
app.use(morgan("dev"));
app.use(router);

// Listening to PORT
app.listen(PORT, () => {
  console.log(`Server nyala di http://localhost:${PORT}`);
});
