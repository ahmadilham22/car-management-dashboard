const { Cars } = require('../models')

const createCars = async (req, res) => {
    const { name, price, size, image } = req.body
    // req.body.name, req.body.price, req.body.quantity
    try {
        const newCars = await Cars.create({
            name,
        price,
        size,
        image,
        })

        res.status(201).json({
            status: 'Success',
            data: {
                newCars
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'Failed',
            errors: [err.message]
        })
    }
}

const findCars = async (req, res) => {
    try {
        const findCar = await Cars.findAll()
        res.status(200).json({
            status: 'Success',
            data: {
                findCar
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'Failed',
            errors: [err.message]
        })
    }
}

const findCarsById = async (req, res) => {
    try {
        const findCars = await Cars.findOne({
            where: {
                id: req.params.id
            }
        })
        res.status(200).json({
            status: 'Success',
            data: {
                findCars
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'Failed',
            errors: [err.message]
        })
    }
}

const updateCars = async (req, res) => {
    try {
        const {name, price, size, image } = req.body
        const id = req.params.id
        const car = await Cars.update({
            name,
        price,
        size,
        image,
            
        }, {
            where: {
                id
            }
        })
        res.status(200).json({
            status: 'Success',
            data: {
                id, name, price, size, image
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'Failed',
            errors: [err.message]
        })
    }
}

const deleteCars = async (req, res) => {
    try {
        const id = req.params.id
        await Cars.destroy({
            where: {
                id
            }
        })
        res.status(200).json({
            status: 'Success',
            message: `Cars with id ${id} have been deleted`
        })
    } catch (err) {
        res.status(400).json({
            status: 'Failed',
            errors: [err.message]
        })
    }
}

module.exports = {
    createCars,
    findCars,
    findCarsById,
    updateCars,
    deleteCars
}